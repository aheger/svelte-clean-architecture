// defines interfaces known to the domain

import type { Todo } from '../entities';
import type {
	CreateTodoRequest,
	DeleteTodoRequest,
	RetrieveTodoRequest,
	UpdateTodoRequest,
} from '../dtos';

export interface ICreateTodoAdapter {
	create(arg0: CreateTodoRequest): Promise<Todo>;
}

export interface IRetrieveTodoAdapter {
	retrieve(arg0: RetrieveTodoRequest): Promise<Todo>;
}

export interface IUpdateTodoAdapter {
	update(arg0: UpdateTodoRequest): Promise<Todo>;
}

export interface IDeleteTodoAdapter {
	delete(arg0: DeleteTodoRequest): Promise<void>;
}

export interface IListTodoAdapter {
	list(): Promise<Todo[]>;
}
