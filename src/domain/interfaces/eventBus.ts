// defines interface for domain event bus

import type { Events, EventsPayload } from '../events';

export type Unsubscribe = () => void;
export type Subscriber<T extends Events> = (payload: EventsPayload[T]) => void;

export interface IEventBus {
	publish<T extends Events>(eventName: T, payload: EventsPayload[T]): void;
	subscribe<T extends Events>(eventName: T, handlerFn: Subscriber<T>): Unsubscribe;
}
