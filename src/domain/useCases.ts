// defines use-cases that exist in the domain
// publishes domain events

import type { IEventBus } from './interfaces/eventBus';
import { Events } from './events';

import type {
	CreateTodoRequest,
	DeleteTodoRequest,
	RetrieveTodoRequest,
	UpdateTodoRequest,
} from './dtos';

import type {
	ICreateTodoAdapter,
	IDeleteTodoAdapter,
	IListTodoAdapter,
	IRetrieveTodoAdapter,
	IUpdateTodoAdapter,
} from './interfaces/adapters';

export async function createTodo(
	createAdapter: ICreateTodoAdapter,
	eventBus: IEventBus,
	createTodoRequest: CreateTodoRequest,
) {
	const todo = await createAdapter.create(createTodoRequest);

	eventBus.publish(Events.TODO_CREATED, todo);
}

export async function deleteTodo(
	deleteAdapter: IDeleteTodoAdapter,
	eventBus: IEventBus,
	deleteTodoRequest: DeleteTodoRequest,
) {
	await deleteAdapter.delete(deleteTodoRequest);

	eventBus.publish(Events.TODO_DELETED, deleteTodoRequest);
}

export async function listTodos(repository: IListTodoAdapter, eventBus: IEventBus) {
	const todos = await repository.list();

	eventBus.publish(Events.TODO_LIST, todos);
}

export async function retrieveTodo(
	retrieveAdapter: IRetrieveTodoAdapter,
	eventBus: IEventBus,
	retrieveTodoRequest: RetrieveTodoRequest,
) {
	const todo = await retrieveAdapter.retrieve(retrieveTodoRequest);

	eventBus.publish(Events.TODO_RETRIEVED, todo);
}

export async function updateTodo(
	updateAdapter: IUpdateTodoAdapter,
	eventBus: IEventBus,
	updateTodoRequest: UpdateTodoRequest,
) {
	const updatedTodo = await updateAdapter.update(updateTodoRequest);

	eventBus.publish(Events.TODO_UPDATED, updatedTodo);
}
