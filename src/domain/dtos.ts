// defines DTOs known to the domain

import type { Todo } from './entities';

type TodoBody = Omit<Todo, 'id'>;

export type CreateTodoRequest = TodoBody;
export type RetrieveTodoRequest = string;
export type UpdateTodoRequest = Partial<TodoBody> & { id: string };
export type DeleteTodoRequest = string;
