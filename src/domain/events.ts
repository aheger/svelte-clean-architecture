// defines events known to the domain
import type { Todo } from './entities';
import type { DeleteTodoRequest } from './dtos';

export enum Events {
	TODO_CREATED = 'TODO_CREATED',
	TODO_DELETED = 'TODO_DELETED',
	TODO_LIST = 'TODO_LIST',
	TODO_RETRIEVED = 'TODO_RETRIEVED',
	TODO_UPDATED = 'TODO_UPDATED',
}

export type EventsPayload = {
	[Events.TODO_CREATED]: Todo;
	[Events.TODO_DELETED]: DeleteTodoRequest;
	[Events.TODO_LIST]: Todo[];
	[Events.TODO_RETRIEVED]: Todo;
	[Events.TODO_UPDATED]: Todo;
};
