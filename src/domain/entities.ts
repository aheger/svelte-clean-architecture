// defines entities known to the domain

export type Todo = {
	id: string;
	title: string;
	content?: string;
	done: boolean;
};
