// commands are used by the UI
// they link domain use-cases with adapters and eventBus

import {
	createTodo as createTodoUseCase,
	deleteTodo as deleteTodoUseCase,
	listTodos as listTodosUseCase,
	retrieveTodo as retrieveTodoUseCase,
	updateTodo as updateTodoUseCase,
} from '../domain/useCases';

import type {
	CreateTodoRequest,
	DeleteTodoRequest,
	RetrieveTodoRequest,
	UpdateTodoRequest,
} from '../domain/dtos';

import repository from './repository/instance';
import eventBus from './eventBus/instance';

export async function createTodo(createTodoRequest: CreateTodoRequest) {
	return createTodoUseCase(repository, eventBus, createTodoRequest);
}

export async function retrieveTodo(retrieveTodoRequest: RetrieveTodoRequest) {
	return retrieveTodoUseCase(repository, eventBus, retrieveTodoRequest);
}

export async function updateTodo(updateTodoRequest: UpdateTodoRequest) {
	return updateTodoUseCase(repository, eventBus, updateTodoRequest);
}

export async function deleteTodo(deleteTodoRequest: DeleteTodoRequest) {
	return deleteTodoUseCase(repository, eventBus, deleteTodoRequest);
}

export async function listTodos() {
	return listTodosUseCase(repository, eventBus);
}
