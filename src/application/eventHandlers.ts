// handles domain events and translates them into application state

import eventBus from './eventBus/instance';
import type { Todo } from '../domain/entities';
import { Events } from '../domain/events';
import todoStore from './store';

eventBus.subscribe(Events.TODO_CREATED, (todo) => {
	todoStore.update((storeTodos) => ({
		...storeTodos,
		[todo.id]: todo,
	}));
});

eventBus.subscribe(Events.TODO_DELETED, (todoId) => {
	todoStore.update((storeTodos) => {
		const todoEntriesWithoutDeleted = Object.entries(storeTodos).filter(
			([deleteCandidateId]) => deleteCandidateId !== todoId,
		);
		return Object.fromEntries(todoEntriesWithoutDeleted);
	});
});

eventBus.subscribe(Events.TODO_LIST, (todos) => {
	const storeTodos = todos.reduce((acc, todo) => {
		acc[todo.id] = todo;
		return acc;
	}, {} as { [id: string]: Todo });

	todoStore.set(storeTodos);
});

eventBus.subscribe(Events.TODO_RETRIEVED, (todo) => {
	todoStore.update((storeTodos) => {
		return {
			...storeTodos,
			[todo.id]: todo,
		};
	});
});

eventBus.subscribe(Events.TODO_UPDATED, (todo) => {
	todoStore.update((storeTodos) => {
		return {
			...storeTodos,
			[todo.id]: todo,
		};
	});
});
