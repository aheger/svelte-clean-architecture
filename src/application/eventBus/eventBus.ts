// implements a typesafe application event bus

import type { IEventBus, Subscriber, Unsubscribe } from '../../domain/interfaces/eventBus';
import type { Events, EventsPayload } from '../../domain/events';

export default class EventBus implements IEventBus {
	private subscriptions: Map<Events, Map<Subscriber<any>, Subscriber<any>>> = new Map();

	publish<T extends Events>(eventName: T, payload?: EventsPayload[T]): void {
		const subscribers = this.subscriptions.get(eventName);

		if (!subscribers) {
			return;
		}

		subscribers.forEach((subscriber) => subscriber(payload));
	}

	subscribe<T extends Events>(eventName: T, subscriber: Subscriber<T>): Unsubscribe {
		let eventSubscribers = this.subscriptions.get(eventName);

		if (!eventSubscribers) {
			eventSubscribers = new Map();
			this.subscriptions.set(eventName, eventSubscribers);
		}

		eventSubscribers.set(subscriber, subscriber);

		return () => {
			eventSubscribers!.delete(subscriber);

			if (!eventSubscribers!.size) {
				this.subscriptions.delete(eventName);
			}
		};
	}
}
