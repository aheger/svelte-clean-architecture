// translates from domain to api entities
// enables access to domain entities

import type {
	ICreateTodoAdapter,
	IDeleteTodoAdapter,
	IListTodoAdapter,
	IRetrieveTodoAdapter,
	IUpdateTodoAdapter,
} from '../../domain/interfaces/adapters';

import type {
	CreateTodoRequest,
	DeleteTodoRequest,
	RetrieveTodoRequest,
	UpdateTodoRequest,
} from '../../domain/dtos';

import type { Todo } from '../../domain/entities';

import type { ApiResult } from '../../api/main';
import api from '../../api/main';

const unknownError = { code: 0, message: 'unknown error' };

export default class Repository
	implements
		ICreateTodoAdapter,
		IDeleteTodoAdapter,
		IListTodoAdapter,
		IRetrieveTodoAdapter,
		IUpdateTodoAdapter
{
	private async unwrapApiPayload<T>(apiResult: Promise<ApiResult<T>>): Promise<T> {
		const result = await apiResult;
		if (!result.success) {
			return Promise.reject({ ...(result.error || unknownError) });
		}
		return result.data!;
	}

	create(createTodoRequest: CreateTodoRequest) {
		return this.unwrapApiPayload(api.createTodo(createTodoRequest));
	}

	retrieve(retrieveTodoRequest: RetrieveTodoRequest): Promise<Todo> {
		return this.unwrapApiPayload(api.retrieveTodo(retrieveTodoRequest));
	}

	update(updateTodoRequest: UpdateTodoRequest): Promise<Todo> {
		const { id, ...body } = updateTodoRequest;
		return this.unwrapApiPayload(api.updateTodo(id, body));
	}

	async delete(deleteTodoRequest: DeleteTodoRequest): Promise<void> {
		try {
			await this.unwrapApiPayload(api.deleteTodo(deleteTodoRequest));
			return;
		} catch (ex) {
			throw ex;
		}
	}

	async list(): Promise<Todo[]> {
		return this.unwrapApiPayload(api.listTodos());
	}
}
