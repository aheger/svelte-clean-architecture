// holds application state

import type { Todo } from '../domain/entities';

import type { Writable } from 'svelte/store';
import { writable } from 'svelte/store';

const store: Writable<{ [id: string]: Todo }> = writable({});

export default store;
