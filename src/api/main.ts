// manages requests to the API
// defines API entities

type ApiTodo = {
	id: string;
	title: string;
	content?: string;
	done: boolean;
};

type ApiTodoBody = Omit<ApiTodo, 'id'>;

export type ApiResult<T> = {
	success: boolean;
	error?: {
		code: number;
		message: string;
	};
	data?: T;
};

// fake API via localStorage
export default {
	async listTodos(): Promise<ApiResult<ApiTodo[]>> {
		await waitFor(100);

		return {
			success: true,
			data: Object.values(loadTodos()),
		};
	},

	async createTodo(todoData: ApiTodoBody): Promise<ApiResult<ApiTodo>> {
		await waitFor(100);

		const todos = loadTodos();
		const newTodo: ApiTodo = {
			...todoData,
			id: uuidv4(),
		};
		todos[newTodo.id] = newTodo;
		saveTodos(todos);

		return {
			success: true,
			data: newTodo,
		};
	},

	async updateTodo(id: string, todoData: Partial<ApiTodoBody>): Promise<ApiResult<ApiTodo>> {
		await waitFor(100);

		const todos = loadTodos();
		if (!todos[id]) {
			return {
				success: false,
				error: {
					code: 404,
					message: `Todo ${id} not found.`,
				},
			};
		}

		todos[id] = {
			...todos[id],
			...todoData,
		};
		saveTodos(todos);

		return {
			success: true,
			data: todos[id],
		};
	},

	async retrieveTodo(id: string): Promise<ApiResult<ApiTodo>> {
		await waitFor(100);

		const todos = loadTodos();
		if (!todos[id]) {
			return {
				success: false,
				error: {
					code: 404,
					message: `Todo ${id} not found.`,
				},
			};
		}

		return {
			success: true,
			data: todos[id],
		};
	},

	async deleteTodo(id: string): Promise<ApiResult<ApiTodo>> {
		await waitFor(100);

		const todos = loadTodos();
		if (!todos[id]) {
			return {
				success: true,
			};
		}

		delete todos[id];
		saveTodos(todos);

		return {
			success: true,
		};
	},
};

function loadTodos(): { [id: string]: ApiTodo } {
	const json = localStorage.getItem('TODOS');
	if (!json) {
		const exampleTodo = {
			id: uuidv4(),
			title: 'An example Todo',
			content: 'some todo content',
			done: false,
		};

		const exampleTodos = {
			[exampleTodo.id]: exampleTodo,
		};

		saveTodos(exampleTodos);

		return exampleTodos;
	}

	try {
		return JSON.parse(json);
	} catch (ex) {
		return {};
	}
}

function saveTodos(todos: { [id: string]: ApiTodo }) {
	localStorage.setItem('TODOS', JSON.stringify(todos));
}

function uuidv4() {
	let d = new Date().getTime(); //Timestamp
	let d2 = (typeof performance !== 'undefined' && performance.now && performance.now() * 1000) || 0; //Time in microseconds since page-load or 0 if unsupported

	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
		var r = Math.random() * 16; //random number between 0 and 16
		if (d > 0) {
			//Use timestamp until depleted
			r = (d + r) % 16 | 0;
			d = Math.floor(d / 16);
		} else {
			//Use microseconds since page-load if supported
			r = (d2 + r) % 16 | 0;
			d2 = Math.floor(d2 / 16);
		}
		return (c === 'x' ? r : (r & 0x3) | 0x8).toString(16);
	});
}

function waitFor(duration: number): Promise<void> {
	return new Promise((res) => setTimeout(() => res(), duration));
}
