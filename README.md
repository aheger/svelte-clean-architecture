# svelte-clean-architecture

## Project goals

- Explore clean architecture principle in context of a Svelte application.

## TODO

- implement all use cases in GUI
- prettify GUI
- add user consent to delete use case
- tests

## Architecture Overview

![Architecture Overview](docs/architecture.svg)
